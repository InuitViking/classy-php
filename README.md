## What is Classy?
Classy is a php class library that is simply made to be a kind of "lego" in PHP, just to make life a bit easier.
Not a framework, not a CMS, but more like something in between.
The methods in the classes should be minimal, yet very functional and adaptive, and most importantly, generic and optimised for performance.

Currently, Classy is being developed, and is not 100% *there* yet, but I'm working on it. Any contributions of any kind, especially security, are very appreciated.
I have a list for what I prioritise in Classy, in order:
1. Secure
2. Generic
3. Optimised

## License

    Classy
    Copyright (C) 2018  Angutivik Casper Rúnur Tausen Hansen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.