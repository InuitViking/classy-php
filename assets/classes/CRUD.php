<?php

class CRUD{

	public function Create($table, array $rows, $con){

		global $con;

		$table = $con->real_escape_string($table);

		$columns = '';
		$values = '';

		foreach($rows as $rowName => $rowValue){

			$rowName = $con->real_escape_string($rowName);
			$rowValue = $con->real_escape_string($rowValue);

			$columns .= "$rowName,";
			$values .= "'$rowValue',";

		}

		$columns = substr_replace($columns, "", -1);
		$values = substr_replace($values, "", -1);

		$sql="INSERT INTO $table ($columns) VALUES ($values)";

		$con->query($sql);
	}//end of Create method


	public function Read($table, array $rows, $other=""){

		global $con;

		$table = $con->real_escape_string($table);
		$other = $con->real_escape_string($other);

		$rowsStr ='';

		foreach($rows as $rowName) {

			$rowName = $con->real_escape_string($rowName);

			$rowsStr.= $rowName .",";

		}

		$rowsStr = substr_replace($rowsStr, "", -1);

		$result = $con->query("SELECT $rowsStr FROM $table $other");

		return $result;

	}//end of Read method

	public function Update($table, array $rowArray, $id){

		global $con;

		$table = $con->real_escape_string($table);
		$id = $con->real_escape_string($id);

		$rowsUpdate = '';

		foreach($rowArray as $tableName => $tableValue){

			$tableName = $con->real_escape_string($tableName);
			$tableValue = $con->real_escape_string($tableValue);

			$rowsUpdate .= $tableName."='".$tableValue."',";

		}

		$rowsUpdate = substr_replace($rowsUpdate, "", -1);

		$sql = "UPDATE $table SET $rowsUpdate WHERE id = $id";

		$con->query($sql);

	}//end of Update method

	public function Delete($table,$id){

		global $con;

		$table = $con->real_escape_string($table);
		$id = $con->real_escape_string($id);

		$con->query("DELETE FROM $table WHERE id=$id");

	}//end of Delete method

}
