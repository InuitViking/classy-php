<?php
//A modified version of https://gist.github.com/jonashansen229/4534794
class Database {
	private $_connection;
	private static $_instance; //The single instance
	private $_host 		= "";
	private $_username	= "";
	private $_password	= "";
	private $_database	= "";

	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	// Constructor
	private function __construct() {
		$this->_connection = new mysqli($this->_host, $this->_username,
			$this->_password, $this->_database) or die("Failed to connect to the database. Please contact the web administrator");
	}

	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }

	// Get mysqli connection
	public function getCon() {
		return $this->_connection;
	}
}

?>
