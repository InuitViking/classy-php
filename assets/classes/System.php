<?php

/**
 * This is the System class, which does a bunch of stuff
 */
class System{

	//Sets timezone.
	//Currently, you have to find the timezones yourself: http://php.net/manual/en/timezones.php
	public function TimeZone($zone)
	{
		return date_default_timezone_set($zone);
	}

	//Shows errors
	public function Err()
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	//Routing
	public function Route(){
		global $user;
		global $default;

		# Check if user wants to Logoff
		if(isset($_GET['logoff'])){
			$user->Logoff();
		}

		// Put ?page into a variable
		$page	= '';
		$lio	= '';
		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}

		# Routing
		if(empty($page)){
			// Necessary to make titles work
			header("Location: $default");
		}elseif(!file_exists('pages/'.$page.'.php')){
			if(!file_exists('pages/lio/'.$page.'.php')){
				$page = '404';
			}else{
				if(isset($_SESSION['uid'])){
					if($user->CheckIfLoggedIn($_SESSION['uid']) === true){
						$lio = 'lio/';
					}else{
						$page = '403';
					}
				}else{
					$page = '403';
				}
			}
		}

		//Require and include viewables and pages
		require_once('assets/viewables/'.$lio.'header.php');
		include("pages/$lio$page.php");
		require_once('assets/viewables/'.$lio.'footer.php');
	}# end of Route class

	//SSL
	//REMEMBER TO EDIT .htaccess FILE!
	public function Https($port)
	{
		if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == $port) {

			$_SERVER['HTTPS'] == 'on';
			$_SERVER['SERVER_PORT'] == $port;
		}
	}
}


?>
