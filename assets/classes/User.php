<?php
	class User{

		public function Login($email, $password){

			global $con;
			global $default;
			global $crud;
			// If form isn't empty
			if($email != '' && $password != ''){

				$email = $con->real_escape_string($email);

				// Put information from the user in a variable
				$users = $crud->Read('users', ['id','email','password'], "WHERE email = '$email' LIMIT 1");

				// If user exists
				if(mysqli_num_rows($users) == 1){

					// Loop the user, because objects are nice, and also, this only loops once.
					while($user = $users->fetch_object()){

						// if password matches
						if(password_verify($password, $user->password)){

							// Check if password needs new hash
							if(password_needs_rehash($password, PASSWORD_DEFAULT)){

								// If yes, make new hash
								$newPass = password_hash($password, PASSWORD_DEFAULT);

								//Update table
								$crud->Update('users', ['password' => $newPass], $user->id);
							}//End of password_needs_rehash

							// Put user ID inside the session 'uid' (stands for 'user id')
							// and send the user to the default page. Maybe send to profile?
							$_SESSION['uid'] = $user->id;
							header("Location: $default");

						}else{// End of password_verify($password, $user->password)
							// Password incorrect
							return 'Incorrect email or password!';
						}

					}// End of while

				}else{// end of mysqli_num_rows($users) == 1
					// Email Nonexistent in DB
					return 'Incorrect email or password';
				}

			}else{// End of $email != '' && $password != ''
				// User didn't fill form
				return 'You must fill the form!';
			}

		}// End of Login-method

		public function Logoff(){

			global $default;
			$_SESSION = array();

			if (ini_get("session.use_cookies")) {
				$params = session_get_cookie_params();
				setcookie(session_name(),
				'',
				time() - 42000,
				$params["path"],
				$params["domain"],
				$params["secure"],
				$params["httponly"]
				);
			}

			// Finally, destroy the session.
			session_destroy();
			header("Location: $default");

		}# End of Logoff methd

		public function CheckIfLoggedIn(){

			global $con;
			$loggedIn = false;

			$sessionUID = $con->real_escape_string($_SESSION['uid']);

			if(!empty($sessionUID)){
				$checkIfExists = $crud->Read('users', ['id'], "WHERE id = $sessionUID LIMIT 1");
				if(mysqli_num_rows($checkIfExists) == 1){
					$loggedIn = true;
				}
			}
			return $loggedIn;
		}# end of CheckIfLoggedIn

		public function GetUserInfo(){

			global $con;
			global $crud;
			$sessionUID = $con->real_escape_string($_SESSION['uid']);

			# Need to figure out how to *NOT* get the password column
			# Luckily it's always salted, but it's still a security issue!
			$user = $crud->Read('users', ['*'], "WHERE id = $sessionUID LIMIT 1");

			while($u = $user->fetch_assoc()){
				$userInfo[] = $u;
			}

			return $userInfo;
		}# end of GetUserInfo

	}

?>
