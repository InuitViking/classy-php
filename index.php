<?php
# Start session
session_start();

# Require all classes
foreach (glob("assets/classes/*.php") as $class) {
	require_once("$class");
}

# Call necessary classes
$system	= new System();
$user	= new User();
$crud	= new CRUD();

$system->Err();

# Connect to database
$connection = Database::getInstance();

# ---Global variables--- #
# Default home page
global $default;
$default = 'default-page';
# Database connection for DB requests etc.
global $con;
$con = $connection->getCon();

# ---Global variables end--- #

# The argument is the default home page if no other page is requested
$system->Route();
